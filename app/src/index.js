import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from 'react-redux'
import {PersistGate} from 'redux-persist/integration/react'

import projectStorage from './reducers/rootReducer'

ReactDOM.render(
    <React.StrictMode>
        <Provider store={projectStorage.store}>
            <PersistGate loading={null} persistor={projectStorage.persistor}>
                <App/>
            </PersistGate>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();
