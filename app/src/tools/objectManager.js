export const includes = (obj, value) => {
    let have = false;

    if (Array.isArray(obj)) {
        have = obj.includes(value);
    }
    else if (typeof obj === 'object') {
        have = Object.values(obj).includes(value);
    }

    return have;
};

export const inputFormat = (value) => {
    //test bool
    if (value === "true") value = true;
    if (value === "false") value = false;

    //test int
    if (typeof value === 'string' && parseInt(value) && value.length === parseInt(value).toString().length) value = parseInt(value);

    //test float
    if (typeof value === 'string' && parseFloat(value) && value.length === parseFloat(value).toString().length) value = parseFloat(value);

    return value;
}
