import {useEffect, useState} from "react";
import request from "../tools/ajaxManager";
import {useAuth} from "../context/provideAuth";
import {Button, Alert, Table} from "react-bootstrap";
import {useParams} from "react-router-dom";
import PathsTR from "./pathsTR";
import DictionaryTR from "./dictionaryTR";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const UnregisteredContent = (props) => {
    const [list, setList] = useState([]);
    const params = useParams();
    const auth = useAuth();

    useEffect(() => {
        request(
            `/unregistred/?project=${params.code}`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setList(response.data)
            }
        )
    }, [auth.token])

    const handleDelete = (e, key) => {
        e.preventDefault();

        request(
            `/unregistred/?id=${list[key].id}`,
            'DELETE',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                let tmp = Array.from(list);
                tmp.splice(key, 1);
                setList(tmp);
            }
        )
    }

    return <div>
        <p className={'mt-2 mb-1'}>Незарегистрированные запросы (те, которые пользователи задавали, но система не нашла ответ):</p>
        <Table striped bordered hover responsive>
            <thead>
            <tr>
                <th>#</th>
                <th>Запрос</th>
                <th>Кол-во таких запросов</th>
                <th>Управление</th>
            </tr>
            </thead>
            <tbody>
            {list.length > 0 ? list.map((item, key) => {
                return <tr key={item.id}>
                    <td>{key + 1}</td>
                    <td>{item.text}</td>
                    <td>{item.count}</td>
                    <td>
                        <Button variant={'danger'} onClick={(e) => handleDelete(e, key)}><FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>
                    </td>
                </tr>
            }) : <tr>
                <td colSpan={4}>Необработанные запросы отсутствуют</td>
            </tr>}
            </tbody>
        </Table>
    </div>
}

export default UnregisteredContent;
