import {useEffect, useState} from "react";
import request from "../tools/ajaxManager";
import {useAuth} from "../context/provideAuth";
import {Button, Alert} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const DictionaryTR = (props) => {
    const [dictionaryItem, setDictionaryItem] = useState(props.data);
    const [newPath, setNewPath] = useState(props.data.path.id);
    const [paths, setPaths] = useState(props.paths);
    const [words, setWords] = useState(props.data.words);
    const [isEdit, setIsEdit] = useState(false);
    const [message, setMessage] = useState(null);
    const auth = useAuth();
    const params = useParams();

    useEffect(() => {
        setNewPath(props.data.path.id);
        setDictionaryItem(props.data)
        setWords(props.data.words)
    }, [props.data])
    useEffect(() => {
        setPaths(props.paths)
    }, [props.paths])
    useEffect(() => {
        if (message) setTimeout(() => {
            setMessage(null)
        }, 3000)
    }, [message])

    const handleUpdate = () => {
        if (!newPath || newPath.toString().length === 0) {
            setMessage('Ошибка! Не выбран путь!');
            return;
        }

        let formData = {
            info: dictionaryItem.id,
            path: newPath,
            project: params.code,
        };
        let count = 0, wordsArr = [];
        words.forEach((word) => {
            if (word.length > 0) {
                wordsArr.push(word);
                count++;
            }
        })
        if (count === 0) {
            setMessage('Ошибка! Не заполнены слово или комбинация слов для данного пути!');
            return;
        }
        formData.words = wordsArr;

        request(
            `/path-finder/update`,
            'POST',
            formData,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function () {
                setIsEdit(false)
            }
        )
    }
    const handleDelete = () => {
        request(
            `/path-finder/`,
            'DELETE',
            {info: dictionaryItem.id},
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function () {
                props.onDelete();
            }
        )
    };
    const handleAddParams = () => {
        let tmp = [...words]
        tmp.push('');
        setWords(tmp);
    }

    if (message) return <tr>
        <td colSpan={4}>
            <Alert variant={'danger'}>
                {message}
            </Alert>
        </td>
    </tr>

    if (isEdit) return <tr>
        <td>
            {props.number + 1}
        </td>
        <td>
            <select value={newPath ?? ''} placeholder={'Выберите путь'}
                    onChange={(e) => setNewPath(e.target.value)}
                    required={true} className={'form-control'}>
                <option disabled={true} value={''}>Выберите путь</option>
                {paths.map(path => {
                    return <option key={path.id} value={path.id}>{path.path} - {path.description}</option>
                })}
            </select>
        </td>
        <td>
            {words.map((word, key) => {
                return <div className={"row g-3 align-items-center" + (key === 0 ? '' : ' mt-1')} key={key}>
                    <div className="col-auto">
                        <input className="form-control" onChange={(e) => {
                            let tmp = [...words]
                            tmp[key] = e.target.value
                            setWords(tmp)
                        }} value={word}/>
                    </div>
                    <div className="col-auto">
                        <Button variant={'outline-danger'} type={'button'} onClick={() => {
                            let tmp = [...words];
                            tmp.splice(key, 1);
                            setWords(tmp)
                        }}><FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>
                    </div>
                </div>
            })}
            <div className="row g-3 align-items-center justify-content-center mt-1">
                <div className="col-auto">
                    <Button variant={'outline-dark'} type={'button'} onClick={handleAddParams}><FontAwesomeIcon icon="fa-solid fa-plus"/> Добавить параметр</Button>
                </div>
            </div>
        </td>
        <td>
            <Button variant={'outline-warning'} onClick={() => setIsEdit(false)}><FontAwesomeIcon icon="fa-solid fa-thumbs-down"/> Отмена</Button>
            <Button className={'mx-2'} variant={'outline-success'} onClick={handleUpdate}><FontAwesomeIcon icon="fa-solid fa-save"/> Сохранить</Button>
        </td>
    </tr>

    return <tr>
        <td>
            {props.number + 1}
        </td>
        <td>
            {dictionaryItem.path.path} ({dictionaryItem.path.description})
        </td>
        <td>
            <p>{dictionaryItem.words.join(', ')}</p>
        </td>
        <td>
            <Button variant={'outline-primary'} onClick={() => setIsEdit(true)}><FontAwesomeIcon icon="fa-solid fa-pen"/> Редактировать</Button>
            <Button className={'mx-2'} variant={'outline-danger'} onClick={handleDelete}><FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>
        </td>
    </tr>
}

export default DictionaryTR;
