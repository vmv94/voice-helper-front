import {useEffect, useState} from "react";
import request from "../tools/ajaxManager";
import {useAuth} from "../context/provideAuth";
import {Alert} from "react-bootstrap";
import {useParams} from "react-router-dom";

const TestContent = (props) => {
    const [search, setSearch] = useState('');
    const [answer, setAnswer] = useState(null);
    const params = useParams();
    const auth = useAuth();

    useEffect(() => {
        if (search.length === 0) setAnswer(null)
        else handleSubmit();
    }, [search])
    const handleSubmit = () => {
        request(
            `/path-finder/?test=1`,
            'POST',
            {
                project: params.code,
                text: search,
            },
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setAnswer(response)
            }
        )
    }

    return <div>
        <p>Введите тестовый запрос и посмотрите результат:</p>
        <form onSubmit={e => {
            e.preventDefault();
        }} className={'bg-success bg-opacity-10 p-2 bg-gradient mb-4'}>
            <input value={search} placeholder={'Введите запрос'}
                   onChange={(e) => setSearch(e.target.value)}
                   className={'form-control '}/>
        </form>
        {answer ? (answer.data ? <Alert variant={'primary'}>
            Путь найден!<br/>
            Путь: <b>{answer.data.path}</b><br/>
            Описание: <b>{answer.data.description}</b><br/>
            Требуется авторизация: <b>{answer.data.needAuth ? 'Да' : 'Нет'}</b><br/>
            Параметры: <b>{JSON.stringify(answer.data.parameters)}</b><br/>
        </Alert> : <Alert variant={'danger'}>
            Ошибка обработки запроса. Сообщение от сервера: <b>{answer.message} - пусто</b>
        </Alert>) : <Alert variant={'warning'}>
            Вы не сделали запрос!
        </Alert>}
    </div>
}

export default TestContent;
