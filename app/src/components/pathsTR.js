import {useEffect, useState} from "react";
import request from "../tools/ajaxManager";
import {useAuth} from "../context/provideAuth";
import {Button, Form} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {inputFormat} from "../tools/objectManager";

const PathsTR = (props) => {
    const [path, setPath] = useState(props.data);
    const [newPath, setNewPath] = useState(props.data.path);
    const [description, setDescription] = useState(props.data.description);
    const [needAuth, setNeedAuth] = useState(props.data.needAuth);
    const [params, setParams] = useState([]);
    const [isEdit, setIsEdit] = useState(false);
    const auth = useAuth();

    useEffect(() => {
        setPath(props.data);
        setNewPath(props.data.path)
        setDescription(props.data.description)
    }, [props.data])
    useEffect(() => {
        let tmp = [];
        Object.keys(path.parameters).forEach(key => {
            tmp.push({
                key: key, value: path.parameters[key],
            })
        })
        setParams(tmp)
    }, [path])

    const handleUpdate = () => {
        let formData = {
            id: path.id,
            path: newPath,
            description: description,
            need_auth: needAuth,
            parameters: {},
        };
        params.forEach((param) => {
            if (param.key.length > 0 && param.value.length > 0) {
                formData.parameters[param.key] = inputFormat(param.value);
            }
        })

        request(
            `/path/update`,
            'POST',
            formData,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setIsEdit(false)
                setPath(response.data);
                props.onUpdate(response.data);
            }
        )
    }
    const handleDelete = () => {
        request(
            `/path/`,
            'DELETE',
            {id: path.id},
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function () {
                props.onDelete();
            }
        )
    };
    const handleAddParams = () => {
        let tmp = [...params]
        tmp.push({key: '', value: ''});
        setParams(tmp);
    }

    if (isEdit) return <tr>
        <td>
            {props.number + 1}
        </td>
        <td>
            <input required={true} className="form-control" onChange={(e) => setNewPath(e.target.value)}
                   value={newPath}/>
        </td>
        <td>
            <input required={true} className="form-control" onChange={(e) => setDescription(e.target.value)}
                   value={description}/>
        </td>
        <td>
            {params.map((param, key) => {
                return <div className={"row g-3 align-items-center" + (key === 0 ? '' : ' mt-1')} key={key}>
                    <div className="col-auto">
                        <label className="col-form-label">Название:</label>
                        <input className="form-control" onChange={(e) => {
                            let tmp = [...params];
                            tmp[key].key = e.target.value;
                            setParams(tmp)
                        }} value={param.key}/>
                    </div>
                    <div className="col-auto">
                        <label className="col-form-label">Значение:</label>
                        <input className="form-control" onChange={(e) => {
                            let tmp = [...params];
                            tmp[key].value = e.target.value;
                            setParams(tmp)
                        }} value={param.value.toString()}/>
                    </div>
                    <div className="col-auto">
                        <Button variant={'outline-danger'} type={'button'} onClick={() => {
                            let tmp = [...params];
                            tmp.splice(key, 1);
                            setParams(tmp)
                        }}><FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>
                    </div>
                </div>
            })}
            <div className="row g-3 align-items-center justify-content-center mt-1">
                <div className="col-auto">
                    <Button variant={'outline-dark'} type={'button'} onClick={handleAddParams}><FontAwesomeIcon
                        icon="fa-solid fa-plus"/> Добавить параметр</Button>
                </div>
            </div>
        </td>
        <td>
            <Form.Check
                type={'checkbox'}
                id={`default-auth_` + path.id}
                label={`Необходимость авторизации`}
                onClick={(e) => {setNeedAuth(e.target.checked)}}
            />
        </td>
        <td>
            <div className={'d-flex flex-row align-items-center justify-content-center'}>
                <Button variant={'outline-warning'} className={'mb-2'} onClick={() => setIsEdit(false)}><FontAwesomeIcon
                    icon="fa-solid fa-thumbs-down"/> Отмена</Button>
                <Button className={'mx-2 mb-2'} variant={'outline-success'} onClick={handleUpdate}><FontAwesomeIcon
                    icon="fa-solid fa-save"/> Сохранить</Button>
            </div>
        </td>
    </tr>

    return <tr>
        <td>
            {props.number + 1}
        </td>
        <td>
            {path.path}
        </td>
        <td>
            {path.description}
        </td>
        <td>
            <p>{Object.keys(path.parameters).map(key => {
                return key + ': ' + path.parameters[key];
            }).join('; ')}</p>
        </td>
        <td>
            {path.needAuth ? 'Да' : 'Нет'}
        </td>
        <td>
            <div className={'d-flex flex-row align-items-center justify-content-center'}>
                <Button variant={'outline-primary'} className={'mb-2'} onClick={() => setIsEdit(true)}><FontAwesomeIcon
                    icon="fa-solid fa-pen"/> Редактировать</Button>
                <Button className={'mx-2 mb-2'} variant={'outline-danger'} onClick={handleDelete}><FontAwesomeIcon
                    icon="fa-solid fa-trash"/> Удалить</Button>
            </div>
        </td>
    </tr>
}

export default PathsTR;
