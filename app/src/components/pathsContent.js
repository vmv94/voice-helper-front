import {useEffect, useState} from "react";
import request from "../tools/ajaxManager";
import {useAuth} from "../context/provideAuth";
import {Button, Alert, OverlayTrigger, Table, Tooltip, Form} from "react-bootstrap";
import PathsTR from "./pathsTR";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {inputFormat} from "../tools/objectManager";

const PathsContent = (props) => {
    const [paths, setPaths] = useState([]);
    const [newPath, setNewPath] = useState('');
    const [description, setDescription] = useState('');
    const [params, setParams] = useState([]);
    const [needAuth, setNeedAuth] = useState(false);
    const [message, setMessage] = useState(null);
    const auth = useAuth();

    useEffect(() => {
        request(
            `/path/`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setPaths(response.data)
            }
        )
    }, [auth.token])
    useEffect(() => {
        props.onUpdate(paths)
    }, [paths])
    useEffect(() => {
        if (message) setTimeout(() => setMessage(null), 4000)
    }, [message])

    const handleSubmit = (e) => {
        e.preventDefault();

        let formData = {
            path: newPath,
            description: description,
            need_auth: needAuth,
            parameters: {},
        }, paramsArr = {};
        params.forEach((param) => {
            if (param.key.length > 0 && param.value.length > 0) {
                formData.parameters[param.key] = inputFormat(param.value);
            }
        })
        formData.parameters = paramsArr;

        request(
            `/path/`,
            'POST',
            formData,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                let tmp = [...paths];
                tmp.push(response.data);
                setPaths(tmp);

                setMessage('Путь добавлен в конец списка!')
                setNewPath('');
                setDescription('');
                setParams([]);
            }
        )
    }
    const handleAddParams = () => {
        let tmp = [...params]
        tmp.push({key: '', value: ''});
        setParams(tmp);
    }

    return <div>
        <p>Добавить новый путь:</p>
        {message ? <Alert variant={'success'}>
            {message}
        </Alert> : <form onSubmit={handleSubmit} className={'bg-success bg-opacity-10 p-2 bg-gradient'}>
            <div className="row g-3 align-items-center">
                <div className="col-auto">
                    <OverlayTrigger
                        placement={'bottom'}
                        overlay={<Tooltip>
                                Путь, по которому произойдет переадресация
                            </Tooltip>}>
                        <label className="col-form-label">Адрес:</label>
                    </OverlayTrigger>
                </div>
                <div className="col-auto">
                    <input required={true} className="form-control" onChange={(e) => setNewPath(e.target.value)} value={newPath}/>
                </div>
                <div className="col-auto">
                    <OverlayTrigger
                        placement={'bottom'}
                        overlay={<Tooltip>
                            Описание маршрута
                        </Tooltip>}>
                    <label className="col-form-label">Описание:</label>
                    </OverlayTrigger>
                </div>
                <div className="col-auto">
                    <input required={true} className="form-control" onChange={(e) => setDescription(e.target.value)} value={description}/>
                </div>
                <div className="col-auto">
                    <Form.Check
                        type={'checkbox'}
                        id={`default-auth`}
                        label={`Необходимость авторизации`}
                        onClick={(e) => {setNeedAuth(e.target.checked)}}
                    />
                </div>
                <div className="col-auto">
                    <Button variant={'outline-success'} type={'submit'} onClick={handleSubmit}>
                        <FontAwesomeIcon icon="fa-solid fa-save"/> Сохранить</Button>
                </div>
            </div>
            <p className={'mt-2 mb-0'}>Параметры для пути:</p>
            {params.map((param, key) => {
                return <div className={"row g-3 align-items-center" + (key === 0 ? '' : ' mt-1')} key={key}>
                    <div className="col-auto">
                        <OverlayTrigger
                            placement={'bottom'}
                            overlay={<Tooltip>
                                Название параметра, который будет передаваться
                            </Tooltip>}>
                            <label className="col-form-label">Название:</label>
                        </OverlayTrigger>
                    </div>
                    <div className="col-auto">
                        <input className="form-control" onChange={(e) => {
                            let tmp = [...params];
                            tmp[key].key = e.target.value;
                            setParams(tmp)
                        }}/>
                    </div>
                    <div className="col-auto">
                        <OverlayTrigger
                            placement={'bottom'}
                            overlay={<Tooltip>
                                Описание маршрута
                            </Tooltip>}>
                            <label className="col-form-label">Значение:</label>
                        </OverlayTrigger>
                    </div>
                    <div className="col-auto">
                        <input className="form-control" onChange={(e) => {
                            let tmp = [...params];
                            tmp[key].value = e.target.value;
                            setParams(tmp)
                        }}/>
                    </div>
                    <div className="col-auto">
                        <Button variant={'outline-danger'} type={'button'} onClick={() => {
                            let tmp = [...params];
                            tmp.splice(key, 1);
                            setParams(tmp)
                        }}><FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>
                    </div>
                </div>
            })}
            <div className="row g-3 align-items-center justify-content-center mt-1">
                <div className="col-auto">
                    <Button variant={'outline-dark'} type={'button'} onClick={handleAddParams}>
                        <FontAwesomeIcon icon="fa-solid fa-plus"/> Добавить параметр</Button>
                </div>
            </div>
        </form>}

        <p className={'mt-2 mb-1'}>Действующие пути:</p>
        <Table striped bordered hover responsive>
            <thead>
            <tr>
                <th>#</th>
                <th>Путь</th>
                <th>Описание</th>
                <th>Параметры</th>
                <th>Нужна<br/>авторизация</th>
                <th>Управление</th>
            </tr>
            </thead>
            <tbody>
            {paths.map((path, key) => {
                return <PathsTR data={path} key={key} number={key} onDelete={() => {
                    let tmp = [...paths];
                    tmp.splice(key, 1);
                    setPaths(tmp)
                }} onUpdate={(data) => {
                    let tmp = [...paths];
                    tmp[key] = data;
                    setPaths(tmp)
                }}/>
            })}
            </tbody>
        </Table>
    </div>
}

export default PathsContent;
