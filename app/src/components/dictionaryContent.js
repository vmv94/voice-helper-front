import {useEffect, useState} from "react";
import request from "../tools/ajaxManager";
import {useAuth} from "../context/provideAuth";
import {Button, Alert, Table} from "react-bootstrap";
import {useParams} from "react-router-dom";
import PathsTR from "./pathsTR";
import DictionaryTR from "./dictionaryTR";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const DictionaryContent = (props) => {
    const [dictionary, setDictionary] = useState([]);
    const [paths, setPaths] = useState(props.paths);
    const [selectedPath, setSelectedPath] = useState(null);
    const [newWords, setNewWords] = useState(['']);
    const [message, setMessage] = useState(null);
    const [messageType, setMessageType] = useState(null);
    const params = useParams();
    const auth = useAuth();

    useEffect(() => {
        setPaths(props.paths)
    }, [props.paths])
    useEffect(() => {
        request(
            `/path-finder/project?project=${params.code}`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setDictionary(response.data)
            }
        )
    }, [auth.token])
    useEffect(() => {
        if (message) setTimeout(() => {
            setMessage(null)
            setMessageType(null);
        }, 3000)
    }, [message])

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!selectedPath || selectedPath.toString().length === 0) {
            setMessage('Ошибка! Не выбран путь!');
            setMessageType('danger');
            return;
        }

        let formData = {
            path: selectedPath,
            project: params.code,
            words: [],
        }, words = [], count = 0;
        newWords.forEach((word) => {
            if (word.length > 0) {
                words.push(word);
                count++;
            }
        })
        if (count === 0) {
            setMessage('Ошибка! Не заполнены слово или комбинация слов для данного пути!');
            setMessageType('danger');
            return;
        }
        formData.words = words;

        request(
            `/path-finder/create`,
            'POST',
            formData,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                let tmp = [...dictionary];
                tmp.push(response.data);
                setDictionary(tmp);

                setMessage('Объект дуспешно добавлен в конец списка!');
                setMessageType('success');
                setSelectedPath(null)
                setNewWords([''])
            }
        )
    }
    const handleAddWord = () => {
        let tmp = [...newWords];
        tmp.push('');
        setNewWords(tmp);
    }

    return <div>
        <p>Добавить новую команду:</p>
        {message ? <Alert variant={messageType}>{message}</Alert> : <form onSubmit={handleSubmit} className={'bg-success bg-opacity-10 p-2 bg-gradient'}>
            <div className="row g-3 align-items-center">
                <div className="col-auto">
                    <label className="col-form-label">Выберите путь:</label>
                </div>
                <div className="col-auto">
                    <select value={selectedPath ?? ''} placeholder={'Выберите путь'}
                            onChange={(e) => setSelectedPath(e.target.value)}
                            required={true} className={'form-control'}>
                        <option disabled={true} value={''}>Выберите путь</option>
                        {paths.map(path => {
                            return <option key={path.id} value={path.id}>{path.path} - {path.description}</option>
                        })}
                    </select>
                </div>
                <div className="col-auto">
                    <Button variant={'outline-success'} type={'submit'} onClick={handleSubmit}>
                        <FontAwesomeIcon icon="fa-solid fa-save"/> Сохранить</Button>
                </div>
            </div>
            <p className={'mt-2 mb-0'}>Слово или комбинация слов для определения пути:</p>
            {newWords.map((word, key) => {
                return <div className={"row g-3 align-items-center" + (key === 0 ? '' : ' mt-1')} key={key}>
                    <div className="col-auto">
                        <input className="form-control" onChange={(e) => {
                            let tmp = [...newWords]
                            tmp[key] = e.target.value
                            setNewWords(tmp)
                        }} value={word}/>
                    </div>
                    <div className="col-auto">
                        <Button variant={'outline-danger'} type={'button'} onClick={() => {
                            let tmp = [...newWords];
                            tmp.splice(key, 1);
                            setNewWords(tmp)
                        }}><FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>
                    </div>
                </div>
            })}
            <div className="row g-3 align-items-center justify-content-center mt-1">
                <div className="col-auto">
                    <Button variant={'outline-dark'} type={'button'} onClick={handleAddWord}><FontAwesomeIcon icon="fa-solid fa-plus"/> Добавить слово в
                        комбинацию</Button>
                </div>
            </div>
        </form>}

        <p className={'mt-2 mb-1'}>Действующие пути:</p>
        <Table striped bordered hover responsive>
            <thead>
            <tr>
                <th>#</th>
                <th>Путь</th>
                <th>Комбинация слов</th>
                <th>Управление</th>
            </tr>
            </thead>
            <tbody>
            {dictionary.map((word, key) => {
                return <DictionaryTR data={word} paths={paths} key={key} number={key} onDelete={() => {
                    let tmp = [...dictionary];
                    tmp.splice(key, 1);
                    setDictionary(tmp)
                }} />
            })}
            </tbody>
        </Table>
    </div>
}

export default DictionaryContent;
