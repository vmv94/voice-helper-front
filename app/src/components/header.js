import {connect} from "react-redux";
import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useAuth} from "../context/provideAuth";
import {useNavigate} from "react-router-dom";
import {includes} from "../tools/objectManager";

const Header = (props) => {
    const auth = useAuth();
    const navigate = useNavigate();

    return <Navbar bg="light" expand="lg" collapseOnSelect className={'vw-100'}>
        <Container>
            <Navbar.Brand href="/admin/" onClick={(e) => {
                e.preventDefault();
                navigate('/admin/')
            }}>
                <FontAwesomeIcon icon="fa-solid fa-robot"/>
                <span className={'mx-2'}><b><i>Голосовой помощник</i></b></span>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto" />
                <Nav>
                    {includes(auth.user.roles, 'ROLE_ADMIN') && <Nav.Link href="/admin/users/" onClick={(e) => {
                        e.preventDefault();
                        navigate('/admin/users/');
                    }}>Пользователи</Nav.Link>}
                    {auth.user && <NavDropdown title={auth.user.email} id="basic-nav-dropdown">
                        <NavDropdown.Item href="/admin/user/" onClick={(e) => {
                            e.preventDefault();
                            navigate('/admin/user/')
                        }}>Мои данные</NavDropdown.Item>
                        <NavDropdown.Item href="/admin/user/password" onClick={(e) => {
                            e.preventDefault();
                            navigate('/admin/user/password')
                        }}>Смена пароля</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#" onClick={(e) => {
                            e.preventDefault();
                            props.onLogout();
                            auth.logout();
                            navigate('/')
                        }}>Выход</NavDropdown.Item>
                    </NavDropdown>}
                </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>;
}

export default connect((state /*, ownProps*/) => {
    return state;
}, dispatch => ({
    onLogout: (data) => {
        dispatch({type: 'LOGOUT', payload: data})
    },
}))(Header);
