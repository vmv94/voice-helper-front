import {connect} from "react-redux";
import {Card, Button, Alert} from "react-bootstrap";
import {useState} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {Link, useNavigate} from "react-router-dom";

const Auth = function (props) {
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [message, setMessage] = useState(null);
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();

        setMessage(null);
        if (email.length < 3 || pass.length === 0) {
            setMessage('Заполнены не все поля формы!');
            return;
        }

        let data = {
            email: email,
            password: pass,
        };

        request(
            '/security/login',
            'POST',
            data,
            {},
            (response) => {
                props.onLogin(response.data);
                navigate("/admin/");
            },
            (error) => {
                if (error.message) {
                    setMessage(error.message);
                }
            },
        );
    }

    return <div className={'d-flex flex-column align-items-center justify-content-center w-100 vh-100'}>
        <Card body className={'d-flex flex-column lign-items-center justify-content-center'}>
            <div className={'d-flex flex-column align-items-center justify-content-center w-100'}>
                <FontAwesomeIcon icon="fa-solid fa-robot" className={'mb-2'} size={'lg'}/>
                <p className={'text-center'}>Админ. панель Вашего голосового помощника<br/><b>Вход:</b></p>
            </div>
            <form onSubmit={(e) => handleSubmit(e)}>
                <input required={true} onChange={(e) => {
                    setEmail(e.target.value);
                }} type={'email'} className={'form-control mb-2'} value={email} placeholder={'E-mail'}/>
                <input required={true} onChange={(e) => {
                    setPass(e.target.value);
                }} type={'password'} className={'form-control mb-2'} value={pass} placeholder={'Пароль'}/>
                {message && <Alert variant={'danger'}>
                    {message}
                </Alert>}
                <div className={'d-flex flex-column align-items-center justify-content-center w-100'}>
                    <Button variant="outline-success" type={'submit'} className={'mb-3'}
                            onClick={(e) => handleSubmit(e)}>Войти</Button>
                    <Link to={'/reset'} className={'mb-2'}>Забыли пароль?</Link>
                    <Link to={'/register'} >Регистрация</Link>
                </div>
            </form>
        </Card>
    </div>;
}

export default connect((state /*, ownProps*/) => {
    return state;
}, dispatch => ({
    onLogin: (data) => {
        dispatch({type: 'LOGIN', payload: data})
    },
}))(Auth);
