import {connect} from "react-redux";
import {Card, Button, Alert} from "react-bootstrap";
import {useState} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {Link, useNavigate} from "react-router-dom";

const Register = function (props) {
    const [email, setEmail] = useState('');
    const [pass, setPass] = useState('');
    const [passConfirm, setPassConfirm] = useState('');
    const [message, setMessage] = useState(null);
    const [messageType, setMessageType] = useState('danger');
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();

        setMessage(null);
        if (email.length < 3) {
            setMessageType('danger')
            setMessage('Заполнены не все поля формы!');
            return;
        }


        let data = {
            email: email,
            type: 'reset',
        };

        request(
            '/security/send/password',
            'POST',
            data,
            {},
            (response) => {
                setMessageType('success')
                setMessage('Пароль выслан Вам на почту!')
            },
            (error) => {
                if (error.message) {
                    setMessage(error.message);
                    setMessageType('danger')
                }
            },
        );
    }

    return <div className={'d-flex flex-column align-items-center justify-content-center w-100 vh-100'}>
        <Card body className={'d-flex flex-column lign-items-center justify-content-center'}>
            <div className={'d-flex flex-column align-items-center justify-content-center w-100'}>
                <div className={'d-flex flex-row align-items-center justify-content-between w-100'}>
                    <FontAwesomeIcon icon="fa-solid fa-arrow-left" className={'mb-2'} size={'lg'} onClick={(e) => {
                        navigate(-1);
                    }}/>
                    <FontAwesomeIcon icon="fa-solid fa-robot" className={'mb-2'} size={'lg'}/>
                    <FontAwesomeIcon icon="fa-solid fa-robot" color={'#ffffff'} size={'lg'}/>
                </div>
                <p className={'text-center'}><b>Восстановление пароля:</b></p>
            </div>
            <form onSubmit={(e) => handleSubmit(e)}>
                <input onChange={(e) => {
                    setEmail(e.target.value);
                }} type={'email'} className={'form-control mb-2'} value={email} placeholder={'E-mail'} required={true}/>
                {message && <Alert variant={'danger'}>
                    {message}
                </Alert>}
                <div className={'d-flex flex-column align-items-center justify-content-center w-100'}>
                    <Button variant="outline-success" type={'submit'} className={'mb-3'}
                            onClick={(e) => handleSubmit(e)}>Выслать новый пароль</Button>
                </div>
            </form>
        </Card>
    </div>;
}

export default connect((state /*, ownProps*/) => {
    return state;
}, dispatch => {})(Register);
