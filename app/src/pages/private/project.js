import {Card, Button, Alert, Spinner, Accordion, Breadcrumb, Nav} from "react-bootstrap";
import {useEffect, useState} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {useAuth} from "../../context/provideAuth";
import {useNavigate, useParams} from "react-router-dom";
import {includes} from "../../tools/objectManager";
import PathsContent from "../../components/pathsContent";
import DictionaryContent from "../../components/dictionaryContent";
import TestContent from "../../components/testContent";
import UnregisteredContent from "../../components/unregisteredContent";

const Project = function (props) {
    const [project, setProject] = useState(null);
    const [paths, setPaths] = useState([]);
    const [activaTab, setActivaTab] = useState("0");
    let auth = useAuth();
    let navigate = useNavigate();
    let params = useParams();

    useEffect(() => {
        request(
            `/project/one?code=${params.code}`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setProject(response.data)
            }
        )
    }, [auth.token])
    const handleActive = () => {
        request(
            `/project/active?project=lom888`,
            'POST',
            {code: params.code},
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setProject(response.data)
            }
        )
    }
    const handleDelete = () => {
        request(
            `/project/`,
            'DELETE',
            {code: params.code},
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                navigate(-1)
            }
        )
    }

    const renderContent = () => {
        let content = null;
        switch (activaTab) {
            case '0': default:
                content = <PathsContent onUpdate={setPaths}/>;
                break;
            case '1':
                content = <DictionaryContent paths={paths}/>;
                break;
            case '2':
                content = <TestContent />
                break;
            case '3':
                content = <UnregisteredContent />
                break;
            case '4':
                content = <div className={'d-flex flex-row align-items-center justify-content-between'}>
                    <Button variant="outline-primary"
                            onClick={() => navigate('/admin/project/' + params.code + '/edit')}>
                        <FontAwesomeIcon icon="fa-solid fa-pen"/> Редактировать
                    </Button>
                    <Button variant="outline-warning"
                            onClick={() => handleActive()}>
                        <FontAwesomeIcon icon="fa-solid fa-power-off"/> {project.isActive ? 'Деактивировать' : 'Активировать'}
                    </Button>
                    {includes(auth.user.roles, 'ROLE_ADMIN') && <Button variant="outline-danger"
                                                                        onClick={handleDelete}>
                        <FontAwesomeIcon icon="fa-solid fa-trash"/> Удалить</Button>}
                </div>;
                break;
        }

        return content;
    }

    if (!project) return <div
        className={'d-flex flex-column align-items-center justify-content-center w-100 max-width-1200'}>
        <Spinner animation={'border'}/>
    </div>

    return <div className={'d-flex flex-column align-items-center justify-content-center w-100 max-width-1200'}>
        <Card body className={'d-block p-1 mt-4 mx-2'}>
            <Breadcrumb>
                <Breadcrumb.Item href="/admin/" onClick={(e) => {e.preventDefault(); navigate('/admin/')}}>Главная</Breadcrumb.Item>
                <Breadcrumb.Item active>Проект "{project.name}"</Breadcrumb.Item>
            </Breadcrumb>
            <h3 className={'mb-2 mt-0'}>Проект "{project.name}"</h3>
            <Nav fill variant="tabs" defaultActiveKey="0" activeKey={activaTab} onSelect={(key, e) => setActivaTab(key)} className={'mb-3'}>
                <Nav.Item  eventKey="0">
                    <Nav.Link eventKey="0">Управление путями</Nav.Link>
                </Nav.Item>
                <Nav.Item  eventKey="1">
                    <Nav.Link eventKey="1">Управление словарем</Nav.Link>
                </Nav.Item>
                <Nav.Item  eventKey="2">
                    <Nav.Link eventKey="2">Тестирование запросов</Nav.Link>
                </Nav.Item>
                <Nav.Item  eventKey="3">
                    <Nav.Link eventKey="3">Нераспознанные запросы</Nav.Link>
                </Nav.Item>
                <Nav.Item  eventKey="4">
                    <Nav.Link eventKey="4">Управление проектом</Nav.Link>
                </Nav.Item>
            </Nav>
            {renderContent()}
        </Card>
    </div>;
}

export default Project;
