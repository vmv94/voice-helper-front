import {connect} from "react-redux";
import {Card, Button, Alert, Breadcrumb} from "react-bootstrap";
import {useEffect, useState} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {Link, useNavigate} from "react-router-dom";
import {useAuth} from "../../context/provideAuth";

const UserPasswordChange = function (props) {
    const [pass, setPass] = useState('');
    const [oldPass, setOldPass] = useState('');
    const [confirmPass, setConfirmPass] = useState('');
    const [message, setMessage] = useState(null);
    const [messageType, setMessageType] = useState(null);
    const auth = useAuth();
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();

        if (pass.length < 8 || oldPass.length < 8 || confirmPass.length < 8) {
            setMessage('Длина пароля должна быть 8 и более символов. Заполните все поля формы, пожалуйста!');
            return;
        }
        if (pass !== confirmPass) {
            setMessage('Новые пароли не совпадают!');
            return;
        }

        let data = {
            oldPassword: oldPass,
            newPassword: pass,
            verify: confirmPass,
        };

        request(
            '/security/change/password',
            'POST',
            data,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            (response) => {
                setMessageType('success')
                setMessage('Данные успешно сохранены')
                setPass(''); setOldPass(''); setConfirmPass('');
            },
            (error) => {
                if (error.message) {
                    setMessageType(null)
                    setMessage(error.message);
                }
            },
        );
    }

    return <Card body className={'mt-2 mx-2'}>
        <Breadcrumb>
            <Breadcrumb.Item href="/admin/" onClick={(e) => {e.preventDefault(); navigate('/admin/')}}>Главная</Breadcrumb.Item>
            <Breadcrumb.Item active>Смена пароля</Breadcrumb.Item>
        </Breadcrumb>
        <h3 className={'mb-2 mt-0 text-center'}>Смена пароля</h3>
        <form onSubmit={(e) => handleSubmit(e)}>
            <input required={true} onChange={(e) => {
                setOldPass(e.target.value);
            }} type={'password'} className={'form-control mb-2'} value={oldPass} placeholder={'Старый пароль'}/>
            <input required={true} onChange={(e) => {
                setPass(e.target.value);
            }} type={'password'} className={'form-control mb-2'} value={pass} placeholder={'Новый пароль'}/>
            <input required={true} onChange={(e) => {
                setConfirmPass(e.target.value);
            }} type={'password'} className={'form-control mb-2'} value={confirmPass} placeholder={'Повторите пароль'}/>
            {message && <Alert variant={messageType ?? 'danger'}>
                {message}
            </Alert>}
            <div className={'d-flex flex-column align-items-center justify-content-center w-100'}>
                <Button variant="outline-success" type={'submit'} className={'mb-3'}
                        onClick={(e) => handleSubmit(e)}><FontAwesomeIcon icon="fa-solid fa-save"/> Сохранить</Button>
            </div>
        </form>
    </Card>;
}

export default UserPasswordChange;
