import {connect} from "react-redux";
import {Card, Button, Alert, Breadcrumb} from "react-bootstrap";
import {useEffect, useState} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {Link, useNavigate} from "react-router-dom";
import {useAuth} from "../../context/provideAuth";

const UserData = function (props) {
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState(null);
    const [messageType, setMessageType] = useState(null);
    const auth = useAuth();
    const navigate = useNavigate();

    useEffect(() => {
        if (auth.user && auth.user.email) setEmail(auth.user.email);
    }, [auth.user])

    const handleSubmit = (e) => {
        e.preventDefault();

        if (email.length < 3) {
            setMessage('Заполнены не все поля формы!');
            return;
        }

        let data = {
            email: email,
        };

        request(
            '/security/change/email',
            'POST',
            data,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            (response) => {
                props.onLogin({user: response.data, token: auth.token});
                auth.login({user: response.data, token: auth.token});
                setMessageType('success')
                setMessage('Данные успешно сохранены')
            },
            (error) => {
                if (error.message) {
                    setMessageType(null)
                    setMessage(error.message);
                }
            },
        );
    }

    return <Card body className={'mt-2 mx-2'}>
        <Breadcrumb>
            <Breadcrumb.Item href="/admin/" onClick={(e) => {e.preventDefault(); navigate('/admin/')}}>Главная</Breadcrumb.Item>
            <Breadcrumb.Item active>Мои данные</Breadcrumb.Item>
        </Breadcrumb>
        <h3 className={'mb-2 mt-0 text-center'}>Мои данные</h3>
        <form onSubmit={(e) => handleSubmit(e)}>
            <input required={true} onChange={(e) => {
                setEmail(e.target.value);
            }} type={'email'} className={'form-control mb-2'} value={email} placeholder={'E-mail'}/>
            {message && <Alert variant={messageType ?? 'danger'}>
                {message}
            </Alert>}
            <div className={'d-flex flex-column align-items-center justify-content-center w-100'}>
                <Button variant="outline-success" type={'submit'} className={'mb-3'}
                        onClick={(e) => handleSubmit(e)}><FontAwesomeIcon icon="fa-solid fa-save"/> Сохранить</Button>
            </div>
        </form>
    </Card>;
}

export default connect((state /*, ownProps*/) => {
    return state;
}, dispatch => ({
    onLogin: (data) => {
        dispatch({type: 'LOGIN', payload: data})
    },
}))(UserData);
