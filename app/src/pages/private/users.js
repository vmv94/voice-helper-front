import {Card, Form, Alert, Table, Breadcrumb} from "react-bootstrap";
import {useEffect, useState} from "react";
import request from "../../tools/ajaxManager";
import {useAuth} from "../../context/provideAuth";
import {includes} from "../../tools/objectManager";
import {useNavigate} from "react-router-dom";

const Users = function (props) {
    const [users, setUsers] = useState([]);
    const [projects, setProjects] = useState([]);
    let auth = useAuth();
    const navigate = useNavigate()

    useEffect(() => {
        usersGet();

        if (includes(auth.user.roles, 'ROLE_ADMIN')) request(
            `/project/`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setProjects(response.data)
            }
        )
    }, [auth.token])

    const usersGet = () => {
        request(
            `/user/`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setUsers(response.data)
            }
        );
    }
    const handleChange = (e, key, role) => {
        e.preventDefault();

        let formData = {
            user: users[key].id,
            role: role,
        };

        request(
            `/user/role`,
            'POST',
            formData,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                let tmp = [...users];
                tmp[key] = response.data;
                setUsers(tmp);
            },
            function (error) {

            }
        )
    }
    const handleAddProject = (projectCode, userId) => {
        let formData = {
            code: projectCode,
            manager: userId,
        };

        request(
            `/project/manager`,
            'POST',
            formData,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function () {
                usersGet();
            },
            function (error) {
                console.log(error)
            }
        )
    }

    return <Card body className={'mt-2 mx-2'}>
        <Breadcrumb>
            <Breadcrumb.Item href="/admin/" onClick={(e) => {e.preventDefault(); navigate('/admin/')}}>Главная</Breadcrumb.Item>
            <Breadcrumb.Item active>Пользователи</Breadcrumb.Item>
        </Breadcrumb>
        <h3 className={'mb-2 mt-0 text-center'}>Пользователи</h3>
        <Table striped bordered hover responsive>
            <thead>
            <tr>
                <th>#</th>
                <th>E-mail</th>
                <th>Роль</th>
                <th>Проекты</th>
            </tr>
            </thead>
            <tbody>
            {users.map((user, key) => {
                return <tr key={user.id}>
                    <td>
                        {key + 1}
                    </td>
                    <td>
                        {user.email}
                    </td>
                    <td>
                        <Form.Group className="w-50" controlId="adminCheckbox">
                            <Form.Check type="checkbox" label="Администратор" onChange={(e) => handleChange(e, key, 'ROLE_ADMIN')} checked={includes(user.roles, 'ROLE_ADMIN')} />
                        </Form.Group>
                        <Form.Group className="w-50" controlId="managerCheckbox">
                            <Form.Check type="checkbox" label="Менеджер проекта" onChange={(e) => handleChange(e, key, 'ROLE_MANAGER')} checked={includes(user.roles, 'ROLE_MANAGER')} />
                        </Form.Group>
                    </td>
                    <td className={includes(user.roles, 'ROLE_ADMIN') ? 'table-success' : !includes(user.roles, 'ROLE_MANAGER') ? 'table-danger' : ''}>
                        {includes(user.roles, 'ROLE_MANAGER') ? projects.map(project => {
                            return <Form.Group className="w-50" controlId={user.id + '_' + project.code} key={project.id}>
                                <Form.Check type="checkbox" label={project.name} onChange={(e) => handleAddProject(project.code, user.id)}
                                            checked={!!user.projects.find(connected => connected.id === project.id)} />
                            </Form.Group>
                        }) : includes(user.roles, 'ROLE_ADMIN') ? <p>Администратор имеет доступ ко всем проектам!</p> : <p>У пользователя не достаточно прав</p>}
                    </td>
                </tr>
            })}
            </tbody>
        </Table>
    </Card>;
}

export default Users;
