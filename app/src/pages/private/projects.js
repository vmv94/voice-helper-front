import {Card, Button, Alert, Breadcrumb} from "react-bootstrap";
import {useEffect, useState} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {useAuth} from "../../context/provideAuth";
import {useNavigate} from "react-router-dom";

const Projects = function (props) {
    const [projects, setProjects] = useState([]);
    let auth = useAuth();
    let navigate = useNavigate();

    useEffect(() => {
        request(
            `/project/`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setProjects(response.data)
            }
        )
    }, [auth.token])

    return <Card body className={'p-1 mt-2 mx-2'}>
        <Breadcrumb>
            <Breadcrumb.Item active>Главная</Breadcrumb.Item>
        </Breadcrumb>
        <h3 className={'mb-2 mt-0 text-center'}>Наши приложения</h3>
        <div className={'d-flex flex-row align-items-center justify-content-center w-100 max-width-1200'}>
            {projects.map((project) => {
                return <Card key={project.id} body onClick={() => {
                    navigate('/admin/project/' + project.code)
                }} className={'d-flex flex-column align-items-center justify-content-center p-3 mt-4 mx-2 pointer'}>
                    <h4>{project.name}</h4>
                </Card>
            })}
            {auth.user.roles.includes('ROLE_ADMIN') &&
            <Card body className={'d-flex flex-column align-items-center justify-content-center p-3 mt-4 mx-2'}>
                <Button variant={'outline-success'} onClick={() => {
                    navigate('/admin/project/add')
                }} type={'button'}>
                    <FontAwesomeIcon icon="fa-solid fa-add" className={'mx-2 ml-0'}/>
                    Создать проект
                </Button>
            </Card>}
        </div>
    </Card>;
}

export default Projects;
