import {Card, Button, Alert, Breadcrumb} from "react-bootstrap";
import {useEffect, useState} from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import request from "../../tools/ajaxManager";
import {useAuth} from "../../context/provideAuth";
import {Link, useNavigate, useParams} from "react-router-dom";

const ProjectForm = function (props) {
    const [project, setProject] = useState(null);
    const [name, setName] = useState('');
    const [code, setCode] = useState('');
    const [message, setMessage] = useState(null);
    let auth = useAuth();
    let navigate = useNavigate();
    let params = useParams();

    useEffect(() => {
        if (params.code) request(
            `/project/one?code=${params.code}`,
            'GET',
            null,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                setProject(response.data);
                setName(response.data.name);
                setCode(response.data.code);
            }
        )
    }, [auth.token])

    const handleSubmit = (e) => {
        e.preventDefault();
        if (name.length === 0 || code.length === 0) {
            setMessage('Заполните все поля формы, пожалуйста!');
            return;
        }

        let formData = {
            name: name,
        };
        if (project) {
            formData.new_code = code;
            formData.code = params.code;
        }
        else formData.code = code;

        request(
            `/project/` + (project ? 'update' : ''),
            'POST',
            formData,
            {
                "YT-AUTH-TOKEN": `YourTar ${auth.token}`
            },
            function (response) {
                navigate(project ? '/admin/project/' + code : -1);
            },
            function (error) {
                if (error.message) setMessage(error.message);
                else setMessage('Неизвестная доселе ошибка! Обратитесь к техническому специалисту!');
            }
        )
    }

    return <div className={'d-flex flex-row align-items-center justify-content-center w-100 max-width-1200'}>
        <Card body className={'d-flex flex-grow-1 flex-shrink-1 flex-column align-items-center justify-content-center p-3 mt-4'}>
            <Breadcrumb className={''}>
                <Breadcrumb.Item href="/admin/" onClick={(e) => {e.preventDefault(); navigate('/admin/')}}>Главная</Breadcrumb.Item>
                {project && <Breadcrumb.Item href={"/admin/project/" + params.code} onClick={(e) => {e.preventDefault(); navigate('/admin/project/' + params.code)}}>Проект "{name}"</Breadcrumb.Item>}
                <Breadcrumb.Item active>Форма {project ? 'редактирования' : 'создания'} проекта</Breadcrumb.Item>
            </Breadcrumb>
            <h3 className={'mb-2 mt-0 text-center'}>Форма {project ? 'редактирования' : 'создания'} проекта</h3>
            <p className={'text-center'}>
                <FontAwesomeIcon icon={"fa-solid fa-" + (project ? 'pen' : 'add')}
                                 className={'mx-2 ml-0'}/>
                {project ? 'Редактирование ' : 'Добавление нового '}
                приложения
            </p>
            <form onSubmit={(e) => handleSubmit(e)}>
                <input required={true} onChange={(e) => {
                    setName(e.target.value);
                }} className={'form-control mb-2'} value={name} placeholder={'Название приложения'}/>
                <input required={true} onChange={(e) => {
                    setCode(e.target.value);
                }} className={'form-control mb-2'} value={code} placeholder={'Код приложения'}/>
                {message && <Alert variant={'danger'}>
                    {message}
                </Alert>}
                <div className={'d-flex flex-column align-items-center justify-content-center w-100'}>
                    <Button variant="outline-success" type={'submit'}
                            onClick={(e) => handleSubmit(e)}>
                        <FontAwesomeIcon icon="fa-solid fa-save"/> {project ? 'Сохранить' : 'Создать'}
                    </Button>
                </div>
            </form>
        </Card>
    </div>;
}

export default ProjectForm;
