import {Route, Routes, useNavigate} from "react-router-dom";
import {lazy, Suspense, useEffect} from "react";
import {Card, Spinner} from "react-bootstrap";
import {useAuth} from "../context/provideAuth";
import Header from "../components/header";
import {includes} from "../tools/objectManager";

const NoMatch = lazy(() => import('../pages/NoMatch'));
const Projects = lazy(() => import('../pages/private/projects'));
const Project = lazy(() => import('../pages/private/project'));
const ProjectForm = lazy(() => import('../pages/private/projectForm'));
const UserData = lazy(() => import('../pages/private/userData'));
const UserPasswordChange = lazy(() => import('../pages/private/userPassword'));
const Users = lazy(() => import('../pages/private/users'));

const PrivateLayout = function (props) {
    let auth = useAuth();
    let navigate = useNavigate();

    useEffect(() => {
        if (!auth.user) navigate('/')
    }, [auth])

    return <div className={'d-flex flex-column align-items-center justify-content-start vw-100'}>
        <Header />
        {auth.user && (includes(auth.user.roles, 'ROLE_MANAGER') || includes(auth.user.roles, 'ROLE_ADMIN')) ?
            <Suspense fallback={<Spinner animation="border"/>}>
            <Routes>
                <Route path="/" element={<Projects/>} />
                <Route path="/project/add" element={<ProjectForm/>} />
                <Route path="/project/:code" element={<Project/>} />
                <Route path="/project/:code/edit" element={<ProjectForm/>} />
                <Route path="/user/" element={<UserData/>} />
                <Route path="/user/password" element={<UserPasswordChange/>} />
                <Route path="/users/" element={<Users/>} />
                <Route path="*" element={<NoMatch/>} />
            </Routes>
            </Suspense> : <Card body className={'mt-2 mx-2'}>
                <p>Вам еще не открыт доступ к данной административной панели. Обратитесь к администратору для получения доступа.</p>
            </Card>}
    </div>;
}

export default PrivateLayout;
