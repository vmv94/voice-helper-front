import {lazy, Suspense, useEffect} from "react";
import {Route, Routes, useNavigate} from "react-router-dom";
import {Spinner} from "react-bootstrap";
import {useAuth} from "../context/provideAuth";

const NoMatch = lazy(() => import('../pages/NoMatch'));
const Auth = lazy(() => import('../pages/public/auth'));
const Register = lazy(() => import('../pages/public/register'));
const ResetPassword = lazy(() => import('../pages/public/resetPassword'));

const PublicLayout = function () {
    const auth = useAuth();
    const navigate = useNavigate();

    useEffect(() => {
        if (auth.user) navigate('/admin/')
    }, [auth.user])

    return <div id={'scrollableDiv'} className={''}>
        <Suspense fallback={<Spinner animation="border" />}>
            <Routes>
                <Route path="/" element={<Auth/>} />
                <Route path="/register" element={<Register/>} />
                <Route path="/reset" element={<ResetPassword/>} />
                <Route path="*" element={<NoMatch/>} />
            </Routes>
        </Suspense>
    </div>;
}

export default PublicLayout;
